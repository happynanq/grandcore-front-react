import React from 'react'
import { NavLink } from 'react-router-dom'

const Users = () => {
  return (
    <div className="page-user">
      <div className="page-user-container">
        {/* <!-- profile block --> */}
        <div className="page-user-profile page-user-profile_bottom">
          {/* <!-- photo --> */}
          <div className="page-user-profile__img" />
          {/* <!-- info --> */}
          <div className="page-user-profile__user-wrap">
            {/* <!-- skills and name --> */}
            <h2 className="page-user-profile__title">Богдан Соколов</h2>
            <p className="page-user-profile__skills">
              PHP, JavaScript, Docker, Ruby, Figma, Photoshop Docker, Ruby, Figma, Photoshop,
              Laravel
            </p>
            {/* <!-- status --> */}
            <div className="page-user-status">
              <a href="#" className="page-user-status__rank page-user-status_bottom">
                <img
                  src="../../img/page-user/icon.png"
                  alt=""
                  className="page-user-status__rank-icon"
                />
                Основатель
              </a>
              <p className="page-user-status__rating page-user-status_bottom">Рейтинг: 223</p>
            </div>
            {/* <!-- btn --> */}
            <div className="page-user-btn">
              <button className="page-user-btn__edit" type="button">
                Edit Profile
              </button>
              <a href="https://www.telegram.com" className="page-user-btn__message">
                <img
                  src="..//../img/page-user/telegram.svg"
                  alt=""
                  className="page-user-btn__message_telegram"
                />
                Send a message
              </a>
            </div>
          </div>
        </div>
        {/* <!-- Projects  --> */}
        <div className="page-user-projects ">
          {/* <!-- menu --> */}
          <ul className="page-user-projects-menu">
            {/* <!-- Projects Founded --> */}
            <li className="page-user-projects-menu__item">
              <NavLink
                to="/users/1/founded"
                className="page-user-projects-menu__link page-user-projects-menu__link_active"
              >
                Projects Founded 2
              </NavLink>
            </li>
            {/* <!-- Projects Supported --> */}
            <li className="page-user-projects-menu__item">
              <NavLink to="/users/1/supported" className="page-user-projects-menu__link">
                Projects Supported 23
              </NavLink>
            </li>
            {/* <!-- Projects Liked --> */}
            <li className="page-user-projects-menu__item">
              <NavLink to="/users/1/liked" className="page-user-projects-menu__link">
                Projects Liked 608
              </NavLink>
            </li>
          </ul>
          {/* <!-- projects cards --> */}
          <div className="page-user-cards">
            {/* <!-- card --> */}
            {[1, 2, 3, 4, 5, 6].map((n) => (
              <article className="page-user-card" key={n}>
                <img
                  src={`../../img/page-user/page-user-bg${n}.jpg`}
                  alt=""
                  className="page-user-card__img"
                />
                <div className="page-user-card__content-wrap">
                  <h3 className="page-user-card__title">
                    Мы хотим вывести идеи Open Source на качественно
                  </h3>
                  <p className="page-user-card__text">
                    Мы хотим вывести идеи Open Source на качественно новый уровень, создав фонд и
                    удобную онлайн платформу для работы над любыми открытыми проектами...
                  </p>
                  <NavLink to="#" className="page-user-card__link">
                    Смотреть проект
                  </NavLink>
                </div>
              </article>
            ))}
          </div>
        </div>
      </div>
    </div>
  )
}

export default Users
