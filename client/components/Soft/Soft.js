import React from 'react'

const Soft = () => {
  return (
    <div className="page-list">
      <div className="page-list-content">
        <section className="page-list-content-list-header img">
          <div className="page-list-content-list-header-row">
            <h1 className="page-list-content-list-header-row__h1">Софт</h1>
          </div>
          <div className="page-list-content-list-header-col">
            <div className="page-list-content-list-header-col-1">
              <h3 className="page-list-content-list-header-col-1__h3">
                Найдите единомышленников и работайте над любыми открытыми проектами вместе
              </h3>
            </div>
          </div>
        </section>
        <section className="page-list-content-wrapper">
          <div id="openFilter" className="page-list-content-wrapper-sidebar">
            <div className="page-list-content-wrapper-sidebar-menutitle">
              <h2 className="page-list-content-wrapper-sidebar-menutitle__h2">
                Отфильтровать результаты
              </h2>
            </div>
            <div
              onClick="showHide('openFilter')"
              className="page-list-content-wrapper-sidebar-close"
              role="presentation"
            >
              <img src="../../img/close.svg" style={{ margin: '18px' }} alt="close" />
            </div>

            <div className="page-list-content-wrapper-sidebar-filters">
              <ul>
                <li className="page-list-content-wrapper-sidebar-filters-section-title">
                  Софт{' '}
                  <img
                    className="page-list-content-wrapper-sidebar-filters-section-title-icon"
                    src="../../img/list-marker.svg"
                    alt="list-marker"
                  />{' '}
                </li>

                <li className="page-list-content-wrapper-sidebar-filters-section-title">
                  Игры{' '}
                  <img
                    className="page-list-content-wrapper-sidebar-filters-section-title-icon"
                    src="../../img/list-marker.svg"
                    alt="list-marker"
                  />
                  <ul>
                    <li>Mobile</li>
                    <li>Action RPG</li>
                    <li>Role-playing</li>
                    <li>MMO RPG</li>
                    <li>Strategy</li>
                    <li>Simulation</li>
                  </ul>
                </li>

                <li className="page-list-content-wrapper-sidebar-filters-section-title">
                  Онлайн-сервисы{' '}
                  <img
                    className="page-list-content-wrapper-sidebar-filters-section-title-icon"
                    src="../../img/list-marker.svg"
                    alt="list-marker"
                  />
                  <ul>
                    <li>Windows</li>
                    <li>Hack</li>
                    <li>Security</li>
                    <li>Corporate</li>
                    <li>Social</li>
                    <li>Linux</li>
                  </ul>
                </li>

                <li className="page-list-content-wrapper-sidebar-filters-section-title">
                  Изделия{' '}
                  <img
                    className="page-list-content-wrapper-sidebar-filters-section-title-icon"
                    src="../../img/list-marker.svg"
                    alt="list-marker"
                  />{' '}
                </li>

                <li className="page-list-content-wrapper-sidebar-filters-section-title">
                  Тексты{' '}
                  <img
                    className="page-list-content-wrapper-sidebar-filters-section-title-icon"
                    alt="list-marker"
                    src="../../img/list-marker.svg"
                  />{' '}
                </li>
              </ul>
            </div>
          </div>
          <div className="page-list-content-wrapper-conteiner">
            <div className="page-list-content-wrapper-conteiner-top">
              <div
                role="presentation"
                onClick="showHide('openFilter')"
                className="page-list-content-wrapper-conteiner-top-filtercontaiber-chosefilter"
              >
                <div className="page-list-content-wrapper-conteiner-top-filtercontaiber-chosefilter-field">
                  <div className="page-list-content-wrapper-conteiner-top-filtercontaiber-chosefilter-field-icon" />
                </div>
              </div>
              <div className="page-list-content-wrapper-conteiner-top-search">
                <div className="page-list-content-wrapper-conteiner-top-search-field">
                  <div className="page-list-content-wrapper-conteiner-top-search-field-icon" />
                  <input
                    className="page-list-content-wrapper-conteiner-top-search-field-input"
                    placeholder="Поиск"
                  />
                </div>
              </div>
              <div className="page-list-content-wrapper-conteiner-top-filtercontaiber-makefilter">
                <div className="page-list-content-wrapper-conteiner-top-filtercontaiber-makefilter-field">
                  <div className="page-list-content-wrapper-conteiner-top-filtercontaiber-makefilter-field-icon" />
                </div>
              </div>
            </div>

            <div className="page-list-content-wrapper-conteiner-grid">
              <div className="page-list-content-wrapper-conteiner-row-col">
                <div className="page-list-content-wrapper-conteiner-row-col-imgconteiner">
                  <img
                    className="page-list-content-wrapper-conteiner-row-col-imgconteiner-img"
                    src="../../img/list-card-1.png"
                    alt="list-card"
                  />
                </div>
                <div
                  className="page-list-content-wrapper-conteiner-row-col-inside"
                  style={{ margin: 0 }}
                >
                  <h4
                    className="page-list-content-wrapper-conteiner-row-col-inside__h4"
                    style={{ margin: 0 }}
                  >
                    Мы хотим вывести идеи Open Source на качественно
                  </h4>
                  <p
                    className="page-list-content-wrapper-conteiner-row-col-inside-description"
                    style={{ margin: 0 }}
                  >
                    Мы хотим вывести идеи Open Source на качественно новый уровень, создав фонд и
                    удобную онлайн платформу...
                  </p>
                </div>
              </div>
              <div className="page-list-content-wrapper-conteiner-row-col">
                <div className="page-list-content-wrapper-conteiner-row-col-imgconteiner">
                  <img
                    className="page-list-content-wrapper-conteiner-row-col-imgconteiner-img"
                    src="../../img/list-card-1.png"
                    alt="list-card"
                  />
                </div>
                <div
                  className="page-list-content-wrapper-conteiner-row-col-inside"
                  style={{ margin: 0 }}
                >
                  <h4
                    className="page-list-content-wrapper-conteiner-row-col-inside__h4"
                    style={{ margin: 0 }}
                  >
                    Мы хотим вывести идеи Open Source на качественно
                  </h4>
                  <p
                    className="page-list-content-wrapper-conteiner-row-col-inside-description"
                    style={{ margin: 0 }}
                  >
                    Мы хотим вывести идеи Open Source на качественно новый уровень, создав фонд и
                    удобную онлайн платформу...
                  </p>
                </div>
              </div>
              <div className="page-list-content-wrapper-conteiner-row-col">
                <div className="page-list-content-wrapper-conteiner-row-col-imgconteiner">
                  <img
                    className="page-list-content-wrapper-conteiner-row-col-imgconteiner-img"
                    src="../../img/list-card-1.png"
                    alt="list-card"
                  />
                </div>
                <div
                  className="page-list-content-wrapper-conteiner-row-col-inside"
                  style={{ margin: 0 }}
                >
                  <h4
                    className="page-list-content-wrapper-conteiner-row-col-inside__h4"
                    style={{ margin: 0 }}
                  >
                    Мы хотим вывести идеи Open Source на качественно
                  </h4>
                  <p
                    className="page-list-content-wrapper-conteiner-row-col-inside-description"
                    style={{ margin: 0 }}
                  >
                    Мы хотим вывести идеи Open Source на качественно новый уровень, создав фонд и
                    удобную онлайн платформу...
                  </p>
                </div>
              </div>
              <div className="page-list-content-wrapper-conteiner-row-col">
                <div className="page-list-content-wrapper-conteiner-row-col-imgconteiner">
                  <img
                    className="page-list-content-wrapper-conteiner-row-col-imgconteiner-img"
                    src="../../img/list-card-1.png"
                    alt="list-card"
                  />
                </div>
                <div
                  className="page-list-content-wrapper-conteiner-row-col-inside"
                  style={{ margin: 0 }}
                >
                  <h4
                    className="page-list-content-wrapper-conteiner-row-col-inside__h4"
                    style={{ margin: 0 }}
                  >
                    Мы хотим вывести идеи Open Source на качественно
                  </h4>
                  <p
                    className="page-list-content-wrapper-conteiner-row-col-inside-description"
                    style={{ margin: 0 }}
                  >
                    Мы хотим вывести идеи Open Source на качественно новый уровень, создав фонд и
                    удобную онлайн платформу...
                  </p>
                </div>
              </div>
            </div>

            <div className="page-list-content-wrapper-conteiner-row-download">
              <a className="page-list-content-wrapper-conteiner-row-download-btn" href="#">
                Загрузи ещё
              </a>
            </div>
          </div>
        </section>
      </div>
    </div>
  )
}

export default Soft
