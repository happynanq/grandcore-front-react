// TODO: Перенести обработчики в обёрточную компоненту, сделать запросы на сервер, получить ответ, записать jwt в редакс(мб в локалсторейдж), по желанию сделать атрибут value в инпуты
// TODO: так же нужно разбить эту компоненту на инпуты и другой jsx :D
// TODO: GL работяге, взявшемуся за это :D

import React from 'react'
// import { useRouteMatch } from 'react-router-dom'

import Auth from './Auth'

const AuthContainer = ({ uri }) => {
  const submitHandler = async (a, b) => {
    try {
      console.log('Пошёл запрос по трубам. ', '\n', a, '///', '\n', b)
      const body = JSON.stringify(b)
      console.log(body)

      const response = fetch('/api/token/', {
        method: 'POST',
        body
      })
      const data = await response
      if (!response.ok) {
        // !Временное решение
        // eslint-disable-next-line no-throw-literal
        throw 'Bad Request'
      }
      console.log(data)
    } catch (error) {
      console.log(error)
    }
  }

  return (
    <>
      <Auth submitHandler={submitHandler} uri={uri} />
    </>
  )
}

export default AuthContainer
