import React from 'react'
import Head from '../head'
// import wave from '../assets/images/wave.jpg'

const Home = () => {
  return (
    <div>
      <Head title="Hello" />

      <div className="page-main">
        <section className="page-main-header page-container img">
          <h1 className="page-main-header__title">Демократическая платформа полного цикла</h1>
          <div className="page-main-header-row">
            <div className="page-main-header-col-1">
              <h3 className="page-main-header-col-1__subtitle">
                Найдите единомышленников и работайте над любыми открытыми проектами вместе
              </h3>

              <div className="page-main-header-col-1__actions">
                <a className="btn btn--big" href="project-new.html">
                  Предложить проект
                </a>
                <a className="btn btn--big btn--green" href="donat.html">
                  Стать спонсором
                </a>
              </div>
            </div>

            <div className="page-main-header-col-2">
              <div className="page-main-header-col-2__watch">
                <button className="btn btn--round btn--transparent" id="play-video" type="button" />
                <label className="play-video-btn-label" htmlFor="#play-video">
                  Смотреть видео
                </label>
              </div>
            </div>
          </div>
        </section>

        <section className="page-main-about page-container page-main-section page-main-section--gray">
          <h2 className="page-main-section__title line-prev">О фонде</h2>
          <h3 className="page-main-section__subtitle">
            Мы объединяем разных специалистов связанных одной идеей!
          </h3>
          <div className="page-main-about-content">
            <div className="page-main-about-content-block__wrapper">
              <div className="page-main-about-content-block">
                <h4 className="page-main-about-content-block__title">
                  <span>1.</span>
                  Что представляет из себя проект?
                </h4>
                <p className="page-main-about-content-block__text">
                  Мы хотим вывести идеи Open Source на качественно новый уровень, создав фонд и
                  удобную онлайн платформу для работы над любыми открытыми проектами: общественно
                  значимыми онлайн Мы хотим вывести идеи Open Source на качественно новый уровень,
                  создав фонд и удобную онлайн платформу для работы над любыми открытыми проектами:
                  общественно значимыми онлайн
                </p>
              </div>

              <div className="page-main-about-content-block">
                <h4 className="page-main-about-content-block__title">
                  <span>2.</span>
                  Как принимаются решения?
                </h4>
                <p className="page-main-about-content-block__text">
                  Мы хотим вывести идеи Open Source на качественно новый уровень, создав фонд и
                  удобную онлайн платформу для работы над любыми открытыми проектами: общественно
                  значимыми онлайн
                </p>
              </div>
            </div>

            <div className="page-main-about-content-block__wrapper">
              <div className="page-main-about-content-block">
                <h4 className="page-main-about-content-block__title">
                  <span>3.</span>
                  Лицензия и устройство фонда
                </h4>
                <p className="page-main-about-content-block__text">
                  Мы хотим вывести идеи Open Source на качественно новый уровень, создав фонд и
                  удобную онлайн платформу для работы над любыми открытыми проектами: общественно
                  значимыми онлайн
                </p>
              </div>

              <div className="page-main-about-content-block">
                <h4 className="page-main-about-content-block__title">
                  <span>4.</span>
                  Как зарабатываем и как тратим?
                </h4>
                <p className="page-main-about-content-block__text">
                  Мы хотим вывести идеи Open Source на качественно новый уровень, создав фонд и
                  удобную онлайн платформу для работы над любыми открытыми проектами: общественно
                  значимыми онлайн Мы хотим вывести идеи Open Source на качественно новый уровень,
                  создав фонд и удобную онлайн платформу для работы над любыми открытыми проектами:
                  общественно значимыми онлайн
                </p>
              </div>
            </div>
          </div>
        </section>

        <section className="page-main-projects page-container page-main-section page-main-section--gray">
          <h2 className="page-main-section__subtitle">Главные проекты</h2>

          <div className="page-main-projects__wrapper">
            <div className="page-main-projects-item">
              <img
                className="page-main-projects-item__img"
                src="img/list-card-6.png"
                alt="Project 1"
              />

              <div className="page-main-projects-item__wrapper">
                <h3 className="page-main-projects-item__title">
                  Мы хотим вывести идеи Open Source на качественно{' '}
                </h3>
                <p className="page-main-projects-item__text">
                  Мы хотим вывести идеи Open Source на качественно новый уровень, создав фонд и
                  удобную онлайн платформу для работы над любыми открытыми проектами...
                </p>
                <a href="#" className="page-main-projects-item__link link--after-row">
                  Смотреть проект
                </a>
              </div>
            </div>

            <div className="page-main-projects-item">
              <img
                className="page-main-projects-item__img"
                src="img/list-card-5.png"
                alt="Project 1"
              />

              <div className="page-main-projects-item__wrapper">
                <h3 className="page-main-projects-item__title">
                  Мы хотим вывести идеи Open Source на качественно{' '}
                </h3>
                <p className="page-main-projects-item__text">
                  Мы хотим вывести идеи Open Source на качественно новый уровень, создав фонд и
                  удобную онлайн платформу для работы над любыми открытыми проектами...
                </p>
                <a href="#" className="page-main-projects-item__link link--after-row">
                  Смотреть проект
                </a>
              </div>
            </div>

            <div className="page-main-projects-item">
              <img
                className="page-main-projects-item__img"
                src="img/list-card-4.png"
                alt="Project 1"
              />

              <div className="page-main-projects-item__wrapper">
                <h3 className="page-main-projects-item__title">
                  Мы хотим вывести идеи Open Source на качественно{' '}
                </h3>
                <p className="page-main-projects-item__text">
                  Мы хотим вывести идеи Open Source на качественно новый уровень, создав фонд и
                  удобную онлайн платформу для работы над любыми открытыми проектами...
                </p>
                <a href="#" className="page-main-projects-item__link link--after-row">
                  Смотреть проект
                </a>
              </div>
            </div>

            <div className="page-main-projects-item">
              <img
                className="page-main-projects-item__img"
                src="img/list-card-7.png"
                alt="Project 1"
              />

              <div className="page-main-projects-item__wrapper">
                <h3 className="page-main-projects-item__title">
                  Мы хотим вывести идеи Open Source на качественно{' '}
                </h3>
                <p className="page-main-projects-item__text">
                  Мы хотим вывести идеи Open Source на качественно новый уровень, создав фонд и
                  удобную онлайн платформу для работы над любыми открытыми проектами...
                </p>
                <a href="#" className="page-main-projects-item__link link--after-row">
                  Смотреть проект
                </a>
              </div>
            </div>

            <div className="page-main-projects-item">
              <img
                className="page-main-projects-item__img"
                src="img/list-card-8.png"
                alt="Project 1"
              />

              <div className="page-main-projects-item__wrapper">
                <h3 className="page-main-projects-item__title">
                  Мы хотим вывести идеи Open Source на качественно{' '}
                </h3>
                <p className="page-main-projects-item__text">
                  Мы хотим вывести идеи Open Source на качественно новый уровень, создав фонд и
                  удобную онлайн платформу для работы над любыми открытыми проектами...
                </p>
                <a href="#" className="page-main-projects-item__link link--after-row">
                  Смотреть проект
                </a>
              </div>
            </div>

            <div className="page-main-projects-item">
              <img
                className="page-main-projects-item__img"
                src="img/list-card-9.png"
                alt="Project 1"
              />

              <div className="page-main-projects-item__wrapper">
                <h3 className="page-main-projects-item__title">
                  Мы хотим вывести идеи Open Source на качественно{' '}
                </h3>
                <p className="page-main-projects-item__text">
                  Мы хотим вывести идеи Open Source на качественно новый уровень, создав фонд и
                  удобную онлайн платформу для работы над любыми открытыми проектами...
                </p>
                <a href="#" className="page-main-projects-item__link link--after-row">
                  Смотреть проект
                </a>
              </div>
            </div>
          </div>

          <a className="page-main-projects__all link--after-row" href="list.html">
            Смотреть все
          </a>
        </section>

        <section className="page-main-stat page-container page-main-section">
          <div className="page-main-stat-item">
            <p className="page-main-stat-item__value">1 281 029 $</p>
            <p className="page-main-stat-item__description">Собрали за все время</p>
          </div>

          <div className="page-main-stat-item">
            <p className="page-main-stat-item__value">8 920 $</p>
            <p className="page-main-stat-item__description">Текущий фонд</p>
          </div>

          <div className="page-main-stat-item">
            <p className="page-main-stat-item__value">5 000 $</p>
            <p className="page-main-stat-item__description">Дефицит бюджета</p>
          </div>

          <div className="page-main-stat-item">
            <p className="page-main-stat-item__value">150 $</p>
            <p className="page-main-stat-item__description">Всего проектов</p>
          </div>

          <div className="page-main-stat-item">
            <p className="page-main-stat-item__value">21 890 $</p>
            <p className="page-main-stat-item__description">Открытых задач</p>
          </div>
        </section>

        <section className="page-main-donate page-container page-main-section">
          <h2 className="page-main-section__title line-prev">помощь проекту</h2>
          <h3 className="page-main-section__subtitle">Пожертвования</h3>

          <div className="page-main-donate-list">
            <div className="page-main-donate-list__col">
              <div className="page-main-donate-item">
                <p className="page-main-donate-item__text">
                  Мы хотим вывести идеи Open Source на качественно новый уровень, создав фонд и
                  удобную онлайн платформу для работы над любыми.
                </p>
                <div>
                  <span className="page-main-donate-item__cost">178$</span>
                  <span className="page-main-donate-item__date">08.08.2020</span>
                </div>
              </div>

              <div className="page-main-donate-item">
                <p className="page-main-donate-item__text">
                  Мы хотим вывести идеи Open Source на качественно новый уровень, создав фонд и
                  удобную онлайн платформу для работы над любыми открытыми проектами.Мы хотим
                  вывести идеи Open Source на качественно новый уровень, создав фонд и удобную
                  онлайн платформу для работы над любыми открытыми проектами.Мы хотим вывести идеи
                  Open Source на качественно новый уровень, создав фонд и удобную онлайн платформу
                  для работы над любыми открытыми проектами.Мы хотим вывести идеи Open Source на
                  качественно новый уровень, создав фонд и удобную онлайн платформу для работы над
                  любыми открытыми проектами. Cоздав фонд и удобную онлайн платформу для работы над
                  любыми открытыми проектами.
                </p>
                <div>
                  <span className="page-main-donate-item__cost">49$</span>
                  <span className="page-main-donate-item__date">08.08.2020</span>
                </div>
              </div>
            </div>

            <div className="page-main-donate-list__col">
              <div className="page-main-donate-item">
                <p className="page-main-donate-item__text">
                  Мы хотим вывести идеи Open Source на качественно новый уровень, создав фонд и
                  удобную онлайн платформу для работы над любыми открытыми проектами.Мы хотим
                  вывести идеи Open Source на качественно новый уровень, создав фонд и удобную
                  онлайн платформу для работы над любыми открытыми проектами.
                </p>
                <div>
                  <span className="page-main-donate-item__cost">49$</span>
                  <span className="page-main-donate-item__date">08.08.2020</span>
                </div>
              </div>

              <div className="page-main-donate-item">
                <p className="page-main-donate-item__text">
                  Мы хотим вывести идеи Open Source на качественно новый уровень, создав фонд и
                  удобную онлайн платформу для работы над любыми.
                </p>
                <div>
                  <span className="page-main-donate-item__cost">133$</span>
                  <span className="page-main-donate-item__date">08.08.2020</span>
                </div>
              </div>

              <div className="page-main-donate-item">
                <p className="page-main-donate-item__text">
                  Мы хотим вывести идеи Open Source на качественно новый уровень, создав фонд и
                  удобную онлайн платформу для работы над любыми открытыми проектами.Мы хотим
                  вывести идеи Open Source на качественно новый уровень, создав фонд и удобную
                  онлайн платформу для работы над любыми открытыми проектами.
                </p>
                <div>
                  <span className="page-main-donate-item__cost">12$</span>
                  <span className="page-main-donate-item__date">08.08.2020</span>
                </div>
              </div>
            </div>
          </div>

          <h3 className="page-main-section__subtitle">Хочу помочь проекту</h3>
          <a href="donat.html" className="page-main-donate__action btn btn--green btn--big">
            Стать cпонсором
          </a>
        </section>

        <section className="page-main-contributors page-container page-main-section page-main-section--gray">
          <h2 className="page-main-section__title line-prev">помощь проекту</h2>
          <h3 className="page-main-section__subtitle">Участники</h3>

          <div className="page-main-contributors">
            <div className="page-main-contributors-list">
              <div className="page-main-contributors-list-col">
                <div className="page-main-contributors-item">
                  <div className="page-main-contributors-item-info__wrapper">
                    <a href="user-page.html">
                      <img
                        src="img/contributors/contributor1.jpg"
                        alt="avatar"
                        className="page-main-contributors-item-info__avatar"
                      />
                    </a>
                    <div className="page-main-contributors-item-info">
                      <a href="user-page.html">
                        <p className="page-main-contributors-item-info__name">Богдан Соколов</p>
                      </a>
                      <p className="page-main-contributors-item-info__status page-main-contributors-item-info__status--owner">
                        Основатель
                      </p>
                    </div>
                  </div>
                  <div className="page-main-contributors-item-stack">
                    <p className="page-main-contributors-item-stack__title">Компетенции</p>
                    <p className="page-main-contributors-item-stack__value">
                      PHP, JavaScript, Docker, Ruby, Figma, Photoshop
                    </p>
                  </div>
                </div>

                <div className="page-main-contributors-item">
                  <div className="page-main-contributors-item-info__wrapper">
                    <a href="user-page.html">
                      <img
                        src="img/contributors/contributor2.jpg"
                        alt="avatar"
                        className="page-main-contributors-item-info__avatar"
                      />
                    </a>
                    <div className="page-main-contributors-item-info">
                      <a href="user-page.html">
                        <p className="page-main-contributors-item-info__name">Полина Медведева</p>
                      </a>
                      <p className="page-main-contributors-item-info__status">Участник</p>
                    </div>
                  </div>
                  <div className="page-main-contributors-item-stack">
                    <p className="page-main-contributors-item-stack__title">Компетенции</p>
                    <p className="page-main-contributors-item-stack__value">
                      PHP, JavaScript, Docker, Ruby, Figma, Photoshop PHP, JavaScript, Docker, Ruby,
                      Figma, Photoshop
                    </p>
                  </div>
                </div>
              </div>

              <div className="page-main-contributors-list-col">
                <div className="page-main-contributors-item">
                  <div className="page-main-contributors-item-info__wrapper">
                    <a href="user-page.html">
                      <img
                        src="img/contributors/contributor3.jpg"
                        alt="avatar"
                        className="page-main-contributors-item-info__avatar"
                      />
                    </a>
                    <div className="page-main-contributors-item-info">
                      <a href="user-page.html">
                        <p className="page-main-contributors-item-info__name">Ангелина Соловова</p>
                      </a>
                      <p className="page-main-contributors-item-info__status">Участник</p>
                    </div>
                  </div>

                  <div className="page-main-contributors-item-stack">
                    <p className="page-main-contributors-item-stack__title">Компетенции</p>
                    <p className="page-main-contributors-item-stack__value">
                      PHP, JavaScript, Docker, Ruby, Figma, Photoshop PHP, JavaScript, Docker, Ruby,
                      Figma, Photoshop
                    </p>
                  </div>
                </div>

                <div className="page-main-contributors-item">
                  <div className="page-main-contributors-item-info__wrapper">
                    <a href="user-page.html">
                      <img
                        src="img/contributors/contributor4.jpg"
                        alt="avatar"
                        className="page-main-contributors-item-info__avatar"
                      />
                    </a>
                    <div className="page-main-contributors-item-info">
                      <a href="user-page.html">
                        <p className="page-main-contributors-item-info__name">Ангелина Соловова</p>
                      </a>
                      <p className="page-main-contributors-item-info__status page-main-contributors-item-info__status--sponsor">
                        Спонсор
                      </p>
                    </div>
                  </div>

                  <div className="page-main-contributors-item-stack">
                    <p className="page-main-contributors-item-stack__title">Компетенции</p>
                    <p className="page-main-contributors-item-stack__value">
                      PHP, JavaScript, Docker, Ruby, Figma, Photoshop
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  )
}

export default Home
