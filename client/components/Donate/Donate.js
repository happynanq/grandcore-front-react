import React from 'react'

const Donate = () => {
  return (
    <div className="page-donat">
      <section className="page-donat-stat page-container page-donat-section">
        <div className="page-donat-stat-item">
          <p className="page-donat-stat-item__value">1 281 029 $</p>
          <p className="page-donat-stat-item__description">Собрали за все время</p>
        </div>

        <div className="page-donat-stat-item">
          <p className="page-donat-stat-item__value">8 920 $</p>
          <p className="page-donat-stat-item__description">Текущий фонд</p>
        </div>

        <div className="page-donat-stat-item">
          <p className="page-donat-stat-item__value">5 000 $</p>
          <p className="page-donat-stat-item__description">Дефицит бюджета</p>
        </div>

        <div className="page-donat-stat-item page-donat-stat-report">
          <p className="page-donat-stat-item__value page-donat-stat-item__report">Отчёт</p>
          <p className="page-donat-stat-item__description">Спасибо всем спонсорам!</p>
        </div>
      </section>

      <section className="page-donat-paymentsistems page-container page-donat-section">
        <div className="page-donat-paymentsistems-item">
          <div className="page-donat-paymentsistems-item__maincontent">
            <img
              className="page-donat-paymentsistems-item__maincontent_img"
              src="../../img/yandex money.png"
              alt="yandex money"
            />
            <p className="page-donat-paymentsistems-item__maincontent_title">Яндекс.Деньги</p>
          </div>
          <p className="page-donat-paymentsistems-item__description">
            Пожертвовать
            <span className="page-donat-paymentsistems-item__description_marker" />
          </p>
        </div>

        <div className="page-donat-paymentsistems-item">
          <div className="page-donat-paymentsistems-item__maincontent">
            <img
              className="page-donat-paymentsistems-item__maincontent_img"
              src="../../img/bitcoin.png"
              alt="bitcoin"
            />
            <p className="page-donat-paymentsistems-item__maincontent_title">Bitcoin</p>
          </div>
          <p className="page-donat-paymentsistems-item__description">
            Пожертвовать
            <span className="page-donat-paymentsistems-item__description_marker" />
          </p>
        </div>

        <div className="page-donat-paymentsistems-item">
          <div className="page-donat-paymentsistems-item__maincontent">
            <img
              className="page-donat-paymentsistems-item__maincontent_img"
              src="../../img/sberBank.png"
              alt="sberBank"
            />
            <p className="page-donat-paymentsistems-item__maincontent_title">Сбербанк</p>
          </div>
          <a className="page-donat-paymentsistems-item__description">
            Пожертвовать <span className="page-donat-paymentsistems-item__description_marker" />
          </a>
        </div>
      </section>

      <section className="page-donat page-container page-donat-section">
        <h2 className="page-donat-section__title line-prev">помощь проекту</h2>
        <h3 className="page-donat-section__subtitle">Пожертвования</h3>

        <div className="page-donat-donate-list">
          <div className="page-donat-donate-list__col">
            <div className="page-donat-donate-item">
              <p className="page-donat-donate-item__text">
                Мы хотим вывести идеи Open Source на качественно новый уровень, создав фонд и
                удобную онлайн платформу для работы над любыми.
              </p>
              <div>
                <span className="page-donat-donate-item__cost">178$</span>
                <span className="page-donat-donate-item__date">08.08.2020</span>
              </div>
            </div>

            <div className="page-donat-donate-item">
              <p className="page-donat-donate-item__text">
                Мы хотим вывести идеи Open Source на качественно новый уровень, создав фонд и
                удобную онлайн платформу для работы над любыми открытыми проектами.Мы хотим вывести
                идеи Open Source на качественно новый уровень, создав фонд и удобную онлайн
                платформу для работы над любыми открытыми проектами.Мы хотим вывести идеи Open
                Source на качественно новый уровень, создав фонд и удобную онлайн платформу для
                работы над любыми открытыми проектами.Мы хотим вывести идеи Open Source на
                качественно новый уровень, создав фонд и удобную онлайн платформу для работы над
                любыми открытыми проектами. Cоздав фонд и удобную онлайн платформу для работы над
                любыми открытыми проектами.
              </p>
              <div>
                <span className="page-donat-donate-item__cost">49$</span>
                <span className="page-donat-donate-item__date">08.08.2020</span>
              </div>
            </div>

            <div className="page-donat-donate-item">
              <p className="page-donat-donate-item__text">
                Мы хотим вывести идеи Open Source на качественно новый уровень, создав фонд и
                удобную онлайн платформу для работы над любыми открытыми проектами.Мы хотим вывести
                идеи Open Source на качественно новый уровень, создав фонд и удобную онлайн
                платформу для работы над любыми открытыми проектами.Мы хотим вывести идеи
              </p>
              <div>
                <span className="page-donat-donate-item__cost">49$</span>
                <span className="page-donat-donate-item__date">08.08.2020</span>
              </div>
            </div>

            <div className="page-donat-donate-item">
              <p className="page-donat-donate-item__text">
                Мы хотим вывести идеи Open Source на качественно новый уровень, создав фонд и
                удобную онлайн платформу для работы над любыми открытыми проектами.Мы хотим вывести
                идеи Open Source на качественно новый уровень, создав фонд и удобную онлайн
                платформу для работы над любыми открытыми проектами.Мы хотим вывести идеи Open
                Source на качественно новый уровень, создав фонд и
              </p>
              <div>
                <span className="page-donat-donate-item__cost">49$</span>
                <span className="page-donat-donate-item__date">08.08.2020</span>
              </div>
            </div>
          </div>

          <div className="page-donat-donate-list__col">
            <div className="page-donat-donate-item">
              <p className="page-donat-donate-item__text">
                Мы хотим вывести идеи Open Source на качественно новый уровень, создав фонд и
                удобную онлайн платформу для работы над любыми открытыми проектами.Мы хотим вывести
                идеи Open Source на качественно новый уровень, создав фонд и удобную онлайн
                платформу для работы над любыми открытыми проектами.
              </p>
              <div>
                <span className="page-donat-donate-item__cost">49$</span>
                <span className="page-donat-donate-item__date">08.08.2020</span>
              </div>
            </div>

            <div className="page-donat-donate-item">
              <p className="page-donat-donate-item__text">
                Мы хотим вывести идеи Open Source на качественно новый уровень, создав фонд и
                удобную онлайн платформу для работы над любыми.
              </p>
              <div>
                <span className="page-donat-donate-item__cost">133$</span>
                <span className="page-donat-donate-item__date">08.08.2020</span>
              </div>
            </div>

            <div className="page-donat-donate-item">
              <p className="page-donat-donate-item__text">
                Мы хотим вывести идеи Open Source на качественно новый уровень, создав фонд и
                удобную онлайн платформу для работы над любыми открытыми проектами.Мы хотим вывести
                идеи Open Source на качественно новый уровень, создав фонд и удобную онлайн
                платформу для работы над любыми открытыми проектами.
              </p>
              <div>
                <span className="page-donat-donate-item__cost">12$</span>
                <span className="page-donat-donate-item__date">08.08.2020</span>
              </div>
            </div>

            <div className="page-donat-donate-item">
              <p className="page-donat-donate-item__text">
                Мы хотим вывести идеи Open Source на качественно новый уровень, создав фонд и
                удобную онлайн платформу для работы над любыми открытыми проектами.Мы хотим вывести
                идеи Open Source на качественно новый уровень, создав фонд и удобную онлайн
                платформу для работы над любыми открытыми проектами.
              </p>
              <div>
                <span className="page-donat-donate-item__cost">12$</span>
                <span className="page-donat-donate-item__date">08.08.2020</span>
              </div>
            </div>
            <div className="page-donat-donate-item">
              <p className="page-donat-donate-item__text">
                Мы хотим вывести идеи Open Source на качественно новый уровень, создав фонд и
                удобную онлайн платформу для работы над любыми открытыми проектами.Мы хотим вывести
                идеи Open Source на качественно новый
              </p>

              <div>
                <span className="page-donat-donate-item__cost">12$</span>
                <span className="page-donat-donate-item__date">08.08.2020</span>
              </div>
            </div>
          </div>
        </div>
      </section>

      <div className="page-donat-downloadmore">
        <button className="page-donat-downloadmore__btn" type="button">
          Загрузи ещё
        </button>
      </div>
    </div>
  )
}

export default Donate
